package com.quovadis.aop.flight.controller;

import com.quovadis.aop.flight.dto.PassengerDto;
import com.quovadis.aop.flight.model.Passenger;
import com.quovadis.aop.flight.service.PassengerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Api(tags = { "Product" })
@RestController
@RequestMapping("/api/v1/passenger")
public class PassengerController {

    @Autowired
    PassengerService passengerService;

    @ApiOperation("Get passenger by id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = Passenger.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 404, message = "Not Found") })
    @GetMapping("/{id}")
    @ResponseStatus(value = HttpStatus.OK)
    public Passenger searchProductById(@PathVariable Long id){
        Passenger passenger = passengerService.searchPassengerById(id);
        return passenger;
    }

    @ApiOperation("Get all passengers.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK", response = Passenger.class),
            @ApiResponse(code = 400, message = "Bad Request"),
            @ApiResponse(code = 404, message = "Not Found") })
    @RequestMapping
    @ResponseStatus(value = HttpStatus.OK)
    public List<Passenger> getAllPassengers() {
        return passengerService.getAllPassengers();
    }

    @ApiOperation("Save passenger")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = Passenger.class),
            @ApiResponse(code = 400, message = "Bad Request") })
    @PostMapping("/save")
    @ResponseStatus(HttpStatus.CREATED)
    public Passenger saveProduct(@Valid @RequestBody PassengerDto passengerDto){
        return passengerService.savePassenger(passengerDto);
    }
}
