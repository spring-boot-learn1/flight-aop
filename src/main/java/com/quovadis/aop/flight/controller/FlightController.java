package com.quovadis.aop.flight.controller;

import com.quovadis.aop.flight.dto.FlightDto;
import com.quovadis.aop.flight.model.Flight;
import com.quovadis.aop.flight.service.FlightService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@Api(tags = { "Flight" })
@RestController
@RequestMapping("/api/v1/flight")
public class FlightController {

    @Autowired
    FlightService flightService;

    @ApiOperation("Save flight")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Created", response = Flight.class),
            @ApiResponse(code = 400, message = "Bad Request") })
    @PostMapping("/save")
    @ResponseStatus(HttpStatus.CREATED)
    public Flight saveFlight(@RequestBody FlightDto flightDto){
        return flightService.saveFlight(flightDto);
    }
}
