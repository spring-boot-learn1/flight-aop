package com.quovadis.aop.flight.model;

import com.quovadis.aop.flight.model.enums.TypeFlight;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@EntityListeners(AuditingEntityListener.class)
public class Flight {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    Long id;

    String company;

    TypeFlight type;

    @OneToMany
    List<Passenger> passengers;

    @Column(name = "created_date", updatable = false, nullable = false)
    @CreatedDate
    private Date createdDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public TypeFlight getType() {
        return type;
    }

    public void setType(TypeFlight type) {
        this.type = type;
    }

    public List<Passenger> getPassengers() {
        return passengers;
    }

    public void setPassengers(List<Passenger> passengers) {
        this.passengers = passengers;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }
}
