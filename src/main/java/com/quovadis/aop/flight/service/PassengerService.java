package com.quovadis.aop.flight.service;

import com.quovadis.aop.flight.dto.PassengerDto;
import com.quovadis.aop.flight.model.Passenger;

import java.util.List;

public interface PassengerService {

    Passenger searchPassengerById(Long id);

    List<Passenger> getAllPassengers();

    Passenger savePassenger(PassengerDto passengerDto);
}
