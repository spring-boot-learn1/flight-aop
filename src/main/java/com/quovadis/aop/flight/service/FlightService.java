package com.quovadis.aop.flight.service;

import com.quovadis.aop.flight.dto.FlightDto;
import com.quovadis.aop.flight.model.Flight;

public interface FlightService {
    Flight saveFlight(FlightDto flightDto);
}
