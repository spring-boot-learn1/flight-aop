package com.quovadis.aop.flight.service.impl;

import com.quovadis.aop.flight.dto.PassengerDto;
import com.quovadis.aop.flight.mapper.PassengerMapper;
import com.quovadis.aop.flight.model.Passenger;
import com.quovadis.aop.flight.repository.PassengerRepository;
import com.quovadis.aop.flight.service.PassengerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PassengerServiceImpl implements PassengerService {

    @Autowired
    PassengerRepository passengerRepository;

    @Autowired
    PassengerMapper passengerMapper;

    @Override
    public Passenger searchPassengerById(Long id) {
        Optional<Passenger> passengerOptional = passengerRepository.findById(id);
        if(passengerOptional.isPresent()) return passengerOptional.get();
        return new Passenger();
    }

    @Override
    public List<Passenger> getAllPassengers() {
        return passengerRepository.findAll();
    }

    @Override
    public Passenger savePassenger(PassengerDto passengerDto) {
        return passengerRepository.save(passengerMapper.convertToEntity(passengerDto));
    }
}
