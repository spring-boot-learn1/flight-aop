package com.quovadis.aop.flight.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LogginAspect {
    private final Logger logger = LoggerFactory.getLogger(LogginAspect.class);

    @Pointcut("within(com.quovadis.aop.flight.controller.PassengerController)")
    public void allPassengerControllerMethods() {
        /*
        Defined PointCut
         */
    }

    @Before("allPassengerControllerMethods()")
    public void loggingAdvice(JoinPoint jointPoint) {
        logger.info("A PassengerController method had been called.");

        logger.info("Info Method: {}, Arg: {}, Target: {}",
                jointPoint.getSignature(),
                jointPoint.getArgs(),
                jointPoint.getTarget());
    }

}
