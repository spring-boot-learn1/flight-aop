package com.quovadis.aop.flight.aspect;

import com.quovadis.aop.flight.dto.FlightDto;
import com.quovadis.aop.flight.dto.PassengerDto;
import com.quovadis.aop.flight.model.enums.TypeFlight;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Aspect
@Component
public class FlightAspect {

    @Around("execution(* com.quovadis.aop.flight.service.impl.FlightServiceImpl.saveFlight(..))")
    public void saveFlightMethodInAspect(ProceedingJoinPoint joinPoint) throws Throwable {
        System.out.println("****FlightAspect.saveFlightMethodInAspect() : " + joinPoint.getSignature().getName() + ": Before Method Execution");
        try {

            FlightDto flightDto = (FlightDto) joinPoint.getArgs()[0];

            //If flight is Premium or Business
            if(flightDto.getType().equals(TypeFlight.Premium) ||
                    flightDto.getType().equals(TypeFlight.Business)){
                flightDto.setPassengers(flightDto.getPassengers().stream().filter(PassengerDto::isVip).collect(Collectors.toList()));;
            }

            joinPoint.proceed(new FlightDto[]{flightDto});
        } finally {
            //Do Something useful, If you have
        }
        System.out.println("****FlightAspect.saveFlightMethodInAspect() : " + joinPoint.getSignature().getName() + ": After Method Execution");

    }
}
