package com.quovadis.aop.flight.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.quovadis.aop.flight.model.enums.TypeFlight;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class FlightDto {

    @NotBlank
    String company;

    @NotBlank
    TypeFlight type;

    @NotEmpty
    List<PassengerDto> passengers;

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public TypeFlight getType() {
        return type;
    }

    public void setType(TypeFlight type) {
        this.type = type;
    }

    public List<PassengerDto> getPassengers() {
        return passengers;
    }

    public void setPassengers(List<PassengerDto> passengers) {
        this.passengers = passengers;
    }
}
