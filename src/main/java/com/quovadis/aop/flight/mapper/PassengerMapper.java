package com.quovadis.aop.flight.mapper;

import com.quovadis.aop.flight.dto.PassengerDto;
import com.quovadis.aop.flight.model.Passenger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PassengerMapper {

    @Autowired
    private ModelMapper modelMapper;

    public PassengerDto convertToDto(Passenger passenger) {
        return modelMapper.map(passenger, PassengerDto.class);
    }

    public Passenger convertToEntity(PassengerDto productDto) {
        return modelMapper.map(productDto, Passenger.class);
    }
}
