package com.quovadis.aop.flight.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = { "com.quovadis.aop.flight.repository" })
@PropertySource({ "classpath:application.properties" })
@EnableJpaAuditing
@ComponentScan({ "com.quovadis.aop.flight.repository" })
public class PersistConfig {
}
