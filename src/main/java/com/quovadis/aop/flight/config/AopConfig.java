package com.quovadis.aop.flight.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@ComponentScan("com.quovadis.aop.flight")
@EnableAspectJAutoProxy
public class AopConfig {
}
