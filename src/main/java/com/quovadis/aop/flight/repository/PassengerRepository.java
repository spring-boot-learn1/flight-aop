package com.quovadis.aop.flight.repository;

import com.quovadis.aop.flight.model.Passenger;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PassengerRepository extends JpaRepository<Passenger, Long> {
}
